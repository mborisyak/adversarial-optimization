#!/bin/bash

for I in {1..5}
do
  sbatch --partition=gpu-1 --gres=gpu:1 --job-name="ABO_NN_$I" --error="ABO_NN_$I.err" --output="ABO_NN_$I.out" \
    adversarial-optimization/run_nn.sh $I &
done

wait
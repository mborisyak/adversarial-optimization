# adversarial-optimization

This repository contains experiments for Rapid Adversarial Optimization study.

## Installation

This repository uses (craynn)[https://gitlab.com/mborisyak/craynn] framework and
(PythiaMill)[https://gitlab.com/mborisyak/pythia-mill] library.

Please, follow the instructions in the corresponding repositories.
Other dependencies are specified in `setup.py`.

## Launching

To launch an experiment:
```bash
python main.py <task> <optimizer> --metrics <metric1> <metric2> ... --devices=<device spec> --repeat=<number of experiments> --budget=<budget>
```
where:
- `task` --- the name of the task to optimize, available tasks are specified in `advopt._list.master_list`;
- `optimizer` --- name of the optimizer, usually, `BO` (Bayesian Optimization) or `RS` (random search),
available optimizers (along with hyper-parameters) are specified in the task specification;
- `metrics` --- list of metrics to use for fine-tuning, usually, in form of `[<divergence>]-[<model>]`, where:
  - `divergence` --- either `JSD` (Jensen-Shannon), `lin-pJSD` or `log-pJSD` (linear/logarithmic capacity pseudo-Jensen-Shannon divergence),
  - `model` --- either `GBDT` (Gradient Boosted Decision Trees from sklearn) or `NN` (Neural Networks).
- `devices` --- comma separated list of devices to use:
  - `gpu<id>:<thread count>[:<memory fraction>]` --- launch `thread count` processes on a GPU with id `id` each with `memory fraction / thread count` memory reserved.
  - `cpu:<thread count>` --- self explanatory;
- `repeat` --- number of experiments to run (varying random seed);
- `budget` --- maximal number of samples to be sampled from a generator, note, that the actual number of samples might exceed this limit.
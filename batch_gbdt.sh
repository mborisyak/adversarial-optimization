#!/bin/bash

for I in {1..10}
do
  sbatch --partition=normal --job-name="ABO_GBDT_$I" --error="ABO_GBDT_$I.err" --output="ABO_GBDT_$I.out" \
    adversarial-optimization/run_gbdt.sh $I &
done

wait
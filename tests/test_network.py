from craynn import *
from advopt import *

def test_networks():
  session = get_cpu_session()

  aclf = dropout_network(session, ndim=(2,), loss_tolerance=1e-3)(
    dense(128),
    (dense(1, activation=linear()), flatten(1))
  )
#!/bin/bash

python main.py XOR BO --metrics JSD-GBDT log-pJSD-GBDT lin-pJSD-GBDT --budget=128000 --devices=cpu:8 --root=RAO --repeat=20 --seed=$1
import numpy as np

from .meta import *
from .search import search

__all__ = [
  'adjusted', 'fixed_sample_size',
  'prob_criterion',
  'diff_criterion',
  'semiprob_criterion'
]

class diff_criterion(object):
  def __init__(self, tolerance):
    self.tolerance = tolerance

  def __call__(self, fs_train, fs_val):
    return np.mean(fs_train) - np.mean(fs_val) - self.tolerance

class prob_criterion(object):
  """
  This criterion treats estimate of average losses as a normally distributed random variables.
  The assumption of normality is valid due to relatively high number of samples.

  Returns `confidence - P(|L_train - L_val| < tolerance)`,
  where L_train, L_val are average losses on train and test samples,
  assumed to be normally distributed.
  """
  def __init__(self, tolerance, confidence):
    self.tolerance = tolerance
    self.confidence = confidence

  def __call__(self, fs_train, fs_val):
    if len(fs_train) < 1:
      return 1

    tolerance, confidence = self.tolerance, self.confidence

    from scipy.stats import norm
    mean_train, std_train = np.mean(fs_train), np.std(fs_train, ddof=1) / np.sqrt(fs_train.shape[0])
    mean_val, std_val = np.mean(fs_val), np.std(fs_val, ddof=1) / np.sqrt(fs_val.shape[0])

    mean = mean_train - mean_val
    std = np.sqrt(std_train ** 2 + std_val ** 2)

    prob_interval = norm.cdf(tolerance / 2, mean, std) - norm.cdf(-tolerance / 2, mean, std)

    return confidence - prob_interval

class semiprob_criterion(object):
  """
  This criterion treats estimate of average losses as a normally distributed random variables.
  The assumption of normality is valid due to relatively high number of samples.

  Returns `max(delta / std_val, delta / std_train) - relative_tolerance`,
  where std_val, std_train are standard deviations of train/validation losses,
  delta is difference between average train and validation losses.
  """
  def __init__(self, relative_tolerance=0.5):
    self.relative_tolerance = relative_tolerance

  def __call__(self, fs_train, fs_val):
    if len(fs_train) < 1:
      return 1

    mean_train, std_train = np.mean(fs_train), np.std(fs_train, ddof=1) / np.sqrt(fs_train.shape[0])
    mean_val, std_val = np.mean(fs_val), np.std(fs_val, ddof=1) / np.sqrt(fs_val.shape[0])

    delta = mean_train - mean_val
    return max(delta / std_train, delta / std_val) - self.relative_tolerance

class fixed_sample_size(object):
  def __init__(self, metric, sample_size):
    self.metric = metric
    self.sample_size = sample_size

  def __call__(self, gen_pos, gen_neg, gen_pos_val=None, gen_neg_val=None):
    if gen_pos_val is None:
      if isinstance(gen_pos, cached_generator):
        raise ValueError('Can not use cached generator for both training and validation!')

      gen_pos_val = cached_generator(gen_pos)

    if gen_neg_val is None:
      if isinstance(gen_neg, cached_generator):
        raise ValueError('Can not use cached generator for both training and validation!')

      gen_neg_val = cached_generator(gen_neg)

    gen_pos_train = gen_pos if isinstance(gen_pos, cached_generator) else cached_generator(gen_pos)
    gen_neg_train = gen_neg if isinstance(gen_neg, cached_generator) else cached_generator(gen_neg)

    X_pos_train, X_neg_train = gen_pos_train(self.sample_size), gen_neg_train(self.sample_size)
    X_pos_val, X_neg_val = gen_pos_val(self.sample_size), gen_neg_val(self.sample_size)

    metric_train, metric_val = self.metric(X_pos_train, X_neg_train, X_pos_val, X_neg_val)
    metric_train, metric_val = np.mean(metric_train), np.mean(metric_val)
    return self.sample_size, (metric_train + metric_val) / 2

class adjusted(object):
  def __init__(self, metric, criterion=diff_criterion(1e-2), xtol=32, search_method='bisect'):
    self.metric = metric
    self.criterion = criterion
    self.xtol = xtol
    self.search_method = search_method

  def __call__(self, gen_pos, gen_neg, gen_pos_val=None, gen_neg_val=None):
    if gen_pos_val is None:
      if isinstance(gen_pos, cached_generator):
        raise ValueError('Can not use cached generator for training and validation!')

      gen_pos_val = cached_generator(gen_pos)

    if gen_neg_val is None:
      if isinstance(gen_neg, cached_generator):
        raise ValueError('Can not use cached generator for training and validation!')

      gen_neg_val = cached_generator(gen_neg)

    gen_pos_train = gen_pos if isinstance(gen_pos, cached_generator) else cached_generator(gen_pos)
    gen_neg_train = gen_neg if isinstance(gen_neg, cached_generator) else cached_generator(gen_neg)

    def m(size):
      if size <= 0:
        return np.log(2), 0

      X_pos_train, X_neg_train = gen_pos_train(size), gen_neg_train(size)
      X_pos_val, X_neg_val = gen_pos_val(size), gen_neg_val(size)

      return self.metric(X_pos_train, X_neg_train, X_pos_val, X_neg_val)

    def target(size):
      ms_train, ms_val = m(size)
      return self.criterion(ms_train, ms_val)

    size0 = search(
      target,
      xtol=self.xtol, method=self.search_method
    )

    metric_train, metric_val = m(size0)
    metric_train, metric_val = np.mean(metric_train), np.mean(metric_val)
    return size0, (metric_train + metric_val) / 2

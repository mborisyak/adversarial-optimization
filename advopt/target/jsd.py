import numpy as np

from ..meta import classfunc
from .utils import combine

__all__ = [
  'jensen_shannon',
  'logloss'
]

def logloss(y, p, eps=1e-6):
  return -(
    y * np.log(p + eps) + (1 - y) * np.log(1 - p + eps)
  )

@classfunc
def jensen_shannon(clf, eps=1e-6):
  def divergence(X_pos, X_neg, X_pos_val, X_neg_val):
    X_train, y_train = combine(X_pos, X_neg)
    X_val, y_val = combine(X_pos_val, X_neg_val)

    clf.fit(X_train, y_train)

    proba_train = clf.predict_proba(X_train)[:, 1]
    proba_val = clf.predict_proba(X_val)[:, 1]

    jsd_train = np.log(2) - logloss(y_train, proba_train, eps=eps)
    jsd_test = np.log(2) - logloss(y_val, proba_val, eps=eps)

    return jsd_train, jsd_test

  return divergence


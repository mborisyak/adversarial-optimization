import numpy as np

__all__ = [
  'Generator',
  'iterator_generator',
  'array_generator',
  'cached_generator'
]

class Generator(object):
  def sample(self, size):
    raise NotImplementedError

  def __call__(self, size):
    return self.sample(size)

class iterator_generator(Generator):
  def __init__(self, it):
    self.it = it

  def sample(self, size):
    return np.array([
      next(self.it)
      for _ in range(size)
    ])

class array_generator(Generator):
  def __init__(self, data):
    self.data = data

  def sample(self, size):
    indx = np.arange(size) % self.data.shape[0]
    return self.data[indx]

class cached_generator(Generator):
  def __init__(self, gen):
    self.gen = gen
    self.data = None

  def sample(self, size):
    if self.data is None:
      self.data = self.gen(size)

      return self.data[:size]

    if self.data.shape[0] < size:
      n = size - self.data.shape[0]

      self.data = np.concatenate([
        self.data,
        self.gen(n)
      ], axis=0)

      return self.data[:size]

    else:
      return self.data[:size]
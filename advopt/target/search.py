import numpy as np

__all__ = [
  'search'
]

class CachedFunction(object):
  def __init__(self, f):
    self.f = f
    self.cache = dict()

  def __call__(self, x):
    x = int(x)

    if x not in self.cache:
      self.cache[x] = self.f(x)

    return self.cache[x]

cached = lambda f: CachedFunction(f)

def search(f, xtol=32, method='bisect'):
  from scipy.optimize import root_scalar

  f = cached(f)

  step = xtol
  a, b = 0, xtol

  while f(b) >= 0:
    step *= 2
    a = b
    b = a + step

  # print('Bracket: (%d, %d)' % (a, b))
  # print('Values: (%.3lf, %.3lf)' % (f(a), f(b)))

  sol = root_scalar(f, bracket=(a, b), method=method, xtol=xtol)

  if not sol.converged:
    raise Exception('Root search did not converge.\n%s' % (sol.flag, ))

  return int(sol.root)








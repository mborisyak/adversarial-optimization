from collections import namedtuple

import numpy as np

OptimizationResults = namedtuple('OptimizationResults', [
  'points', 'values', 'costs', 'seed'
])

def transform(x, space):
  space = np.asarray(space)
  return (x - space[None, :, 0]) / (space[:, 1] - space[:, 0])[None, :]

def reverse_transform(x, space):
  space = np.asarray(space)
  return x * (space[:, 1] - space[:, 0])[None, :] + space[None, :, 0]
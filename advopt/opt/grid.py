import numpy as np

from .meta import reverse_transform

__all__ = [
  'grid_search',
]

class GridSearchOptimizer(object):
  def __init__(self, space, steps, seed=None):
    self.space = np.asarray(space)
    self.models = None
    xs = tuple([
      np.linspace(lower_x, upper_x, num=steps)
      for lower_x, upper_x in space
    ])

    grids = np.meshgrid(*xs)
    self.grid = np.vstack([
      grid.reshape(-1)
      for grid in grids
    ]).T

    self.current = 0

  def name(self):
    return 'GridSearch'

  def ask(self):
    if self.current < self.grid.shape[0]:
      result = self.grid[self.current, :]
      self.current += 1
      return result
    else:
      self.current = 0
      raise StopIteration()

  def tell(self, x, y):
    pass

class grid_search(object):
  def __init__(self, steps):
    self.steps = steps

  def name(self):
    return 'GridSearch'

  def __call__(self, space, seed=None):
    return GridSearchOptimizer(space, self.steps, seed=seed)


from skopt import Optimizer
from inspect import signature, Signature

__all__ = [
  'bayesian_optimization'
]

class BayesianOptimizer(Optimizer):
  def __init__(self, *args, **kwargs):
    super(BayesianOptimizer, self).__init__(*args, **kwargs)

  def name(self):
    return 'BayesianOptimization'

BayesianOptimizer.__init__.__signature__ = signature(Optimizer)

class bayesian_optimization(object):
  def __init__(self, *args, **kwargs):
    self.args = args
    self.kwargs = kwargs

  def name(self):
    return 'BayesianOptimization'

  def __call__(self, space, seed=None):
    return BayesianOptimizer(*self.args, dimensions=space, random_state=seed, **self.kwargs)

original_signature = signature(Optimizer)
new_signature = Signature(parameters=[
  param
  for name, param in original_signature.parameters.items()
  if name not in ['dimensions', 'random_state']
], return_annotation=original_signature.return_annotation)

bayesian_optimization.__init__.__signature__ = new_signature
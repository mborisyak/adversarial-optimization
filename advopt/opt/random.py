import numpy as np

from .meta import reverse_transform

__all__ = [
  'random_search',
]

class RandomSearchOptimizer(object):
  def __init__(self, space, seed=None):
    self.space = np.asarray(space)
    self.models = None

    self.rng = np.random.RandomState(seed=seed)

  def name(self):
    return 'RandomSearch'

  def ask(self):
    return reverse_transform(
      np.random.uniform(size=self.space.shape[0]),
      self.space
    )[0, :]

  def tell(self, x, y):
    pass

class random_search(object):
  def __init__(self):
    pass

  def name(self):
    return 'RandomSearch'

  def __call__(self, space, seed=None):
    return RandomSearchOptimizer(space, seed=seed)


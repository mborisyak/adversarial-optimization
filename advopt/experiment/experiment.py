import os
import multiprocessing as mp

import numpy as np

from advopt.target.meta import cached_generator
from advopt.opt.meta import OptimizationResults

from advopt.target import adjusted, fixed_sample_size, jensen_shannon

__all__ = [
  'experiment',
  'get_experiment_root',
  'load_results'
]

def superseed(seed=None, used_seeds=None):
  rng = np.random.RandomState(seed)

  if used_seeds is None:
    used = set()
  else:
    used = set(used_seeds)

  while True:
    s = rng.randint(0, 2 ** 31 - 1)

    while s in used:
      s = rng.randint(0, 2 ** 31 - 1)

    used.add(s)
    yield s

def worker(task, device, command_queue, result_queue, logs='logs/'):
  from ..meta import apply_with_kwargs, get_kwargs

  try:
    import sys

    if logs is not None:
      sys.stdout = open(
        os.path.join(logs, '%d.out' % (os.getpid(),)),
        mode='w'
      )
      sys.stderr = open(
        os.path.join(logs, '%d.err' % (os.getpid(),)),
        mode='w'
      )
    else:
      sys.stdout = open(os.devnull, mode='w')
      sys.stderr = open(os.devnull, mode='w')
  except:
    pass

  while True:
    try:
      command = command_queue.get()

      if command is None:
        return

      model_name, model_args, budget, n_samples, optimizer_model, seed = command

      master_rng = np.random.RandomState(seed=seed)
      get_seed = lambda: master_rng.randint(0, 2 ** 31, dtype='int32')

      model = apply_with_kwargs(
        task.models(**model_args)[model_name],
        device=device,
        seed=get_seed()
      )
      if n_samples is 'adjusted':
        metric = adjusted(jensen_shannon(model))
      else:
        metric = fixed_sample_size(jensen_shannon(model), n_samples)

      optimizer = optimizer_model(task.search_space(), seed=get_seed())

      task.set_seed(get_seed())
      ground_truth_generator = task.ground_truth_generator()

      ### to avoid spawning two generator instances
      gen_pos_train = cached_generator(ground_truth_generator)
      gen_pos_val = cached_generator(ground_truth_generator)

      known_points = []
      known_values = []
      costs = []

      total_cost = 0

      while True:
        if budget is not None and total_cost >= budget:
          break

        try:
          point = optimizer.ask()
        except StopIteration:
          break

        gen_neg = task.generator(point)

        ### to avoid spawning two generator instances
        gen_neg_train = cached_generator(gen_neg)
        gen_neg_val = cached_generator(gen_neg)

        cost, value = metric(gen_pos_train, gen_neg_train, gen_pos_val, gen_neg_val)
        optimizer.tell(point, value)

        known_points.append(point)
        known_values.append(value)
        costs.append(cost)

        total_cost += cost

      result = OptimizationResults(
        points=np.array(known_points),
        values=np.array(known_values),
        costs=costs,
        seed=seed
      )

      result_queue.put((model_name, result))
    except Exception as e:
      import traceback
      stack = traceback.format_exc()

      result_queue.put((None, (e, stack)))


def load_results(root, name):
  import pickle
  import re

  r = re.compile(r'^%s-\d+[.]pickled$' % (name, ))

  try:
    results = list()
    files = [
      path
      for path in os.listdir(root)
      if r.fullmatch(path) is not None
    ]

    for path in files:
      with open(os.path.join(root, path), 'rb') as f:
        results.append(
          pickle.load(f)
        )

    return results
  except FileNotFoundError:
    return list()

def save_result(root, result, name):
  import pickle
  path = os.path.join(root, '%s-%d.pickled' % (name, result.seed))

  with open(path, 'wb') as f:
    pickle.dump(result, f)

def get_experiment_root(root, task, optimizer, budget):
  return os.path.join(root, '%s-%s-%s' % (task.name(), optimizer.name(), budget))

def experiment(
  root, metrics, optimizer,
  task, budget, repeat,
  model_args=None,
  devices=None,
  progress=lambda x: x,
  seed=None,
  sample_size='adjusted',
  logs=None
):
  import os

  root = get_experiment_root(root, task, optimizer, budget)
  os.makedirs(root, exist_ok=True)

  if logs is not None:
    os.makedirs(logs, exist_ok=True)

  if model_args is None:
    model_args = dict()

  if isinstance(metrics, list) or isinstance(metrics, tuple):
    metrics = {
      name : model_args
      for name in metrics
    }

  results = {
    name : load_results(root, name)
    for name in metrics
  }


  n_tasks = sum([
    max(0, repeat - len(results[name]))
    for name in metrics
  ])

  if n_tasks == 0:
    return results

  used_seeds = [opt_result.seed for name in results for opt_result in results[name]]
  seed_stream = superseed(seed, used_seeds=used_seeds)

  if devices is None:
    devices = ['cpu']

  n_procs = min(len(devices), n_tasks)

  context = mp.get_context('spawn')

  command_queue = context.Queue()
  result_queue = context.Queue()

  procs = [
    context.Process(
      target=worker,
      kwargs=dict(
        task=task,
        device=device,
        command_queue=command_queue,
        result_queue=result_queue,
        logs=logs
      )
    )

    for _, device in zip(range(n_procs), devices)
  ]

  for proc in procs:
    proc.start()

  commands = list()

  for name in metrics:
    left = max(0, repeat - len(results[name]))

    for _ in range(left):
      commands.append(
        (name, model_args, budget, sample_size, optimizer, next(seed_stream))
      )

  import random
  random.shuffle(commands)

  for command in commands:
    command_queue.put(command)

  for _ in range(n_procs):
    command_queue.put(None)

  for _ in progress(range(n_tasks)):
    name, result = result_queue.get()

    if name is None:
      print(result)
    else:
      results[name].append(result)
      save_result(root, result, name)

  for proc in procs:
    proc.join(timeout=5)

    if proc.exitcode is None:
      os.kill(proc.pid, 9)

      print('Process %d was brutally killed.' % (proc.pid, ))

  return results
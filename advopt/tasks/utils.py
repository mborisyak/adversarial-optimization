import numpy as np

__all__ = [
  'rot_matrix',
  'gbdt_models',
  'nn_models'
]

def gbdt_models(ndim, n_estimators=100, max_depth=5):
  from sklearn.ensemble import GradientBoostingClassifier
  from ..classifier import AdaptiveBoosting, logspace, linspace

  return {
    'JSD-GBDT' : lambda : GradientBoostingClassifier(
      n_estimators=n_estimators, max_depth=max_depth
    ),

    'log-pJSD-GBDT' : lambda : AdaptiveBoosting(
          GradientBoostingClassifier(n_estimators=n_estimators, max_depth=max_depth),
          thresholds=logspace(n_estimators)
    ),

    'lin-pJSD-GBDT': lambda : AdaptiveBoosting(
        GradientBoostingClassifier(n_estimators=n_estimators, max_depth=max_depth),
        thresholds=linspace(n_estimators)
    ),
  }

def nn_models(ndim, n_units=128, grad_penalty=1e-2, reg_penalty=1e-3):
  from ..classifier import Dense, Network, GradNetwork, RegNetwork
  from ..classifier.nn.utils import lincapacity, logcapacity

  return {
    'JSD-NN': lambda device: Network(
      net=Dense(ndim=ndim, n0=n_units),
      device=device
    ),

    'lin-pJSD-NN': lambda device: GradNetwork(
      net=Dense(ndim=ndim, n0=n_units),
      penalty=grad_penalty, capacity=lincapacity(),
      device=device,
    ),

    'log-pJSD-NN' : lambda device: GradNetwork(
      net=Dense(ndim=ndim, n0=n_units),
      penalty=grad_penalty, capacity=logcapacity(),
      device=device,
    ),

    'lin-pJSD-reg': lambda device: RegNetwork(
      net=Dense(ndim=ndim, n0=n_units),
      penalty=grad_penalty, capacity=lincapacity(),
      device=device,
    ),

    'log-pJSD-reg': lambda device: RegNetwork(
      net=Dense(ndim=ndim, n0=n_units),
      penalty=grad_penalty, capacity=logcapacity(),
      device=device,
    ),
  }

def rot_matrix(angle):
  return np.array([
    [np.cos(angle), -np.sin(angle)],
    [np.sin(angle), np.cos(angle)],
  ])
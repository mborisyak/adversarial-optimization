import numpy as np

from .meta import Task

__all__ = [
  'PythiaTuneMC'
]

fixed_options = [
  ### seeting default parameters to Monash values
  "Tune:ee = 7",
  "Beams:idA = 11",
  "Beams:idB = -11",
  "Beams:eCM = 91.2",
  "WeakSingleBoson:ffbar2gmZ = on",
  "23:onMode = off",
  "23:onIfMatch = 1 -1",
  "23:onIfMatch = 2 -2",
  "23:onIfMatch = 3 -3",
  "23:onIfMatch = 4 -4",
  "23:onIfMatch = 5 -5",
]

param_names = [
  "TimeShower:alphaSvalue",
  "TimeShower:pTmin",
  "TimeShower:pTminChgQ",

  "StringPT:sigma",
  "StringZ:bLund",
  "StringZ:aExtraSQuark",
  "StringZ:aExtraDiquark",
  "StringZ:rFactC",
  "StringZ:rFactB",

  "StringFlav:probStoUD",
  "StringFlav:probQQtoQ",
  "StringFlav:probSQtoQQ",
  "StringFlav:probQQ1toQQ0",
  "StringFlav:mesonUDvector",
  "StringFlav:mesonSvector",
  "StringFlav:mesonCvector",
  "StringFlav:mesonBvector",
  "StringFlav:etaSup",
  "StringFlav:etaPrimeSup",
  "StringFlav:decupletSup"
]

space = [
  (0.06, 0.25),
  (0.1, 2.0),
  (0.1, 2.0),

  (0.2, 1.0),
  (0.0, 1.0),
  (0.0, 2.0),
  (0.0, 2.0),
  (0.0, 2.0),
  (0.0, 2.0),

  (0.0, 1.0),
  (0.0, 1.0),
  (0.0, 1.0),
  (0.0, 1.0),
  (0.0, 1.0),
  (0.0, 1.0),
  (0.0, 1.0),
  (0.0, 3.0),
  (0.0, 3.0),
  (0.0, 3.0),
  (0.0, 3.0)
]

monash = np.array([
  0.1365,
  0.5,
  0.5,

  0.98,
  0.335,
  0,
  0.97,
  1.32,
  0.885,

  0.217,
  0.081,
  0.915,
  0.0275,
  0.6,
  0.12,
  1,
  0.5,
  0.55,
  0.88,
  2.2
])

class PythiaTuneMC(Task):
  def __init__(self, n_params=None, n_jobs=None, seed=None):
    import pythiamill as pm
    self.detector = pm.utils.TuneMCDetector()

    if n_params is None:
      n_params = len(param_names)

    assert n_params <= len(param_names)
    self.n_params = n_params
    self.n_jobs = n_jobs

    self.seed = seed

    super(PythiaTuneMC, self).__init__(ndim=self.detector.event_size(), seed=seed)

  def set_seed(self, seed=None):
    self.seed = seed

  def name(self):
    return 'Pythia-%d' % (self.n_params, )

  def parameters_names(self):
    return param_names[:self.n_params]

  def search_space(self):
    return space[:self.n_params]

  def solution(self):
    return monash[:self.n_params]

  def ground_truth_generator(self):
    import pythiamill as pm
    params = monash[:self.n_params]


    return pm.CachedPythiaMill(
      detector_factory=self.detector,
      options=pm.config.please_be_quiet + fixed_options + [
        '%s = %lf' % (k, v)
        for k, v in zip(param_names, params)
      ],
      batch_size=32,
      n_workers=self.n_jobs,
      seed=self.seed
    )

  def generator(self, params):
    import pythiamill as pm

    return pm.CachedPythiaMill(
      detector_factory=self.detector,
      options=pm.config.please_be_quiet + fixed_options + [
        '%s = %lf' % (k, v)
        for k, v in zip(param_names, params)
      ],
      batch_size=32,
      n_workers=self.n_jobs,
      seed=self.seed
    )

  def model_parameters(self):
    return dict(
      n_estimators=100,
      max_depth=3,

      n_units=128,
      grad_penalty=5e-2,
    )
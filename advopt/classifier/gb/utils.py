import numpy as np

__all__ = [
  'linspace', 'logspace',
]

def linspace(num):
  return np.linspace(1, 0, num=num)

def logspace(num):
  a = np.linspace(0, 1, num=num)[1:]
  return np.log(a) / np.log(a[0])

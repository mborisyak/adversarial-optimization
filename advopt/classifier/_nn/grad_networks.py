from advopt.classifier.nn.utils import improvement_criterion, lincapacity
from .just_networks import BaseNetwork

__all__ = [
  'AdaptiveGradNetwork',
  'grad_network',
]

class AdaptiveGradNetwork(BaseNetwork):
  def __init__(
    self,
    session, ndim, nn, capacity=lincapacity(),
    optimizer=None, batch_size=32,
    stop_criterion=improvement_criterion(8),
    grad_penalty=1e-2
  ):
    import tensorflow as tf
    from craynn import tf_updates

    if optimizer is None:
      optimizer = tf_updates.adam(learning_rate=5e-4)

    self.capacity = capacity
    self.grad_penalty = tf.constant(grad_penalty, dtype='float32')
    super(AdaptiveGradNetwork, self).__init__(
      session, ndim, nn,
      optimizer=optimizer, batch_size=batch_size,
      stop_criterion=stop_criterion
    )

  def _loss(self, cross_entropy):
    import tensorflow as tf

    grad, = tf.gradients(self.predictions_train, self.X_train)
    self.grad_norm = tf.reduce_mean(grad ** 2)

    zero = tf.constant(0, dtype='float32')

    self.reg_coef = tf.maximum(self.capacity(self.cross_entropy_acc / self.log2), zero)
    return self.cross_entropy_train + self.grad_penalty * self.reg_coef * self.grad_norm

def grad_network(
  session, ndim, capacity=lincapacity(),
  optimizer=None, batch_size=32,
  stop_criterion=improvement_criterion(8),
  grad_penalty=1e-2
):

  def model(*layers):
    return AdaptiveGradNetwork(
      session, ndim=ndim, nn=layers, capacity=capacity,
      optimizer=optimizer, batch_size=batch_size,
      stop_criterion=stop_criterion, grad_penalty=grad_penalty
    )

  return model




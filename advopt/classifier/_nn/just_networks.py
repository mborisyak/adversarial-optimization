import numpy as np
from advopt.classifier.nn.utils import *

__all__ = [
  'BaseNetwork',
  'just_network',
]

class BaseNetwork(object):
  def __init__(
    self,
    session, ndim, nn,
    optimizer=None, batch_size=32,
    stop_criterion=improvement_criterion(8),
    rho=0.99
  ):
    import tensorflow as tf
    from craynn import tf_updates
    from craynn import net, utils, objectives

    if optimizer is None:
      optimizer = tf_updates.adam(learning_rate=5e-4)

    if not isinstance(ndim, tuple):
      ndim = (ndim, )

    self.nn = net((None,) + ndim)(nn)

    self.session = session
    self.batch_size = batch_size

    self.stop_criterion = stop_criterion

    self.X_placeholder = tf.placeholder(shape=(None, ) + ndim, dtype='float32')
    self.y_placeholder = tf.placeholder(shape=(None, ), dtype='float32')

    self.dataset_train = tf.data.Dataset.from_tensor_slices(
      (self.X_placeholder, self.y_placeholder)
    ).repeat().shuffle(buffer_size=32 * batch_size).batch(batch_size)

    self.iterator_train = self.dataset_train.make_initializable_iterator()
    self.X_train, self.y_train = self.iterator_train.get_next()

    self.dataset_test = tf.data.Dataset.from_tensor_slices(
      (self.X_placeholder, )
    ).batch(batch_size).repeat()

    self.iterator_test = self.dataset_test.make_initializable_iterator()
    self.X_test,  = self.iterator_test.get_next()

    self.predictions_train, = self.nn(self.X_train)

    self.predictions_test, = self.nn.mode(deterministic=True)(self.X_test)
    self.proba_test = tf.nn.sigmoid(self.predictions_test)

    self.cross_entropy_train = objectives.logit_binary_crossentropy(self.y_train, self.predictions_train)

    self.log2 = tf.constant(np.log(2), dtype='float32')
    self.rho = tf.constant(rho, dtype='float32')
    self.nrho = tf.constant(1 - rho, dtype='float32')

    self.cross_entropy_acc = tf.Variable(initial_value=self.log2)
    self.cross_entropy_acc_upd = tf.assign(
      self.cross_entropy_acc,
      self.rho * self.cross_entropy_acc + self.nrho * self.cross_entropy_train
    )

    self.loss = self._loss(self.cross_entropy_train)

    # self.loss_acc = tf.Variable(initial_value=self.log2)
    # self.loss_acc_upd = tf.assign(
    #   self.loss_acc,
    #   self.rho * self.loss_acc + self.nrho * self.loss
    # )

    self.optimizer = optimizer(self.loss, self.nn.variables(trainable=True))

    self.initial = utils.variables_like(self.nn.variables())
    self.initial_assign = tf.group([
      tf.assign(init, var)
      for init, var in zip(self.initial, self.nn.variables())
    ])
    self.variables_assign = tf.group([
      tf.assign(var, init)
      for init, var in zip(self.initial, self.nn.variables())
    ])
    self.session.run(self.nn.reset())
    self.session.run(self.initial_assign)

    self.rst = self._rst()
    self.upd = self._upd()

    self.session.run(self.rst)

    self._loss_history = list()

  def _upd(self):
    import tensorflow as tf

    return tf.group([
      self.optimizer.updates(),
      self.cross_entropy_acc_upd,
      # self.loss_acc_upd,
    ])

  def _rst(self):
    import tensorflow as tf

    return tf.group(
      self.optimizer.reset(),
      self.variables_assign,
      # self.loss_acc.initializer,
      self.cross_entropy_acc.initializer
    )

  def _loss(self, cross_entropy):
    return cross_entropy

  def fit(self, X, y, reset=True, progress=None):
    indx = np.random.permutation(X.shape[0])

    self.session.run(
      self.iterator_train.initializer,
      feed_dict={
        self.X_placeholder : X[indx],
        self.y_placeholder : y[indx]
      }
    )

    if reset:
      self.session.run(self.rst)

    n_iterations = X.shape[0] // self.batch_size

    self._loss_history = list()

    pbar = None if progress is None else progress()

    while True:
      epoch_losses = np.ones(shape=(n_iterations, ), dtype='float32')

      if self.stop_criterion(self._loss_history):
        break

      for i in range(n_iterations):
        _, epoch_losses[i] = self.session.run([self.upd, self.loss])

      self._loss_history.append(np.mean(epoch_losses))

      if progress:
        pbar.update()
        pbar.set_description('loss: %.3lf' % (self._loss_history[-1], ))

    self._loss_history = np.array(self._loss_history)
    return self

  def loss_history(self):
    return self._loss_history

  def predict_proba(self, X, flatten=False):
    from craynn.updates import streams

    self.session.run(
      self.iterator_test.initializer,
      feed_dict={
        self.X_placeholder : X
      }
    )

    result = np.ndarray(shape=(X.shape[0], ), dtype='float32')

    for indx in streams.seq(X.shape[0], batch_size=self.batch_size):
      result[indx] = self.session.run(self.proba_test)

    if flatten:
      return result
    else:
      return np.vstack([1 - result, result]).T

  def predict_grid(self, space, steps=20):
    xs = tuple([
      np.linspace(lower_x, upper_x, num=steps)
      for lower_x, upper_x in space
    ])

    grids = np.meshgrid(*xs)
    X = np.vstack([grid.reshape(-1) for grid in grids]).T
    proba = self.predict_proba(X, flatten=True)
    proba = proba.reshape((steps, ) * len(space))

    return xs, proba

def just_network(
  session, ndim,
  optimizer=None, batch_size=32,
  stop_criterion=improvement_criterion(8),
):
  def model(*layers):
    return BaseNetwork(
      session=session, ndim=ndim, nn=layers,
      optimizer=optimizer, batch_size=batch_size,
      stop_criterion=stop_criterion
    )

  return model
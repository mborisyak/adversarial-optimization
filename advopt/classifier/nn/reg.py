import numpy as np

import torch
from torch import nn

from .utils import improvement_criterion, seq
from .utils import lincapacity
from .nn import Network

__all__ = [
  'RegNetwork'
]

LOG2 = np.float32(np.log(2))

class RegNetwork(Network):
  def __init__(
    self, net, penalty=1e-2, capacity=lincapacity(), rho=0.99,
    device='cpu',
    stop_criterion=improvement_criterion(),
    batch_size=32, max_epoches=1024,
    min_iterations_per_epoch=128
  ):
    super(RegNetwork, self).__init__(
      net, device=device,
      stop_criterion=stop_criterion,
      batch_size=batch_size,
      max_epoches=max_epoches,
      min_iterations_per_epoch=min_iterations_per_epoch
    )

    self.ce_acc = LOG2
    self.penalty = penalty
    self.capacity = capacity
    self.rho = rho

  def reset(self):
    super(RegNetwork, self).reset()
    self.ce_acc = LOG2

  def fit(self, X, y, reset=True, progress=None):
    if reset:
      self.reset()

    loss_fn = nn.BCEWithLogitsLoss()

    n_iterations = max(X.shape[0] // self.batch_size, self.min_iterations_per_epoch)
    pbar = progress(total=n_iterations) if progress is not None else None

    for i in range(self.max_epoches):
      epoch_losses = np.zeros(shape=(n_iterations, ), dtype='float32')

      if pbar is not None:
        pbar.reset(n_iterations)
        pbar.set_description('epoch %d' % (len(self._loss_history), ))

      for j in range(n_iterations):
        self.opt.zero_grad()

        indx = np.random.randint(X.shape[0], size=(self.batch_size, ))
        X_batch = torch.tensor(X[indx], dtype=torch.float32, device=self.device, requires_grad=True)
        y_batch = torch.tensor(y[indx], dtype=torch.float32, device=self.device, requires_grad=False)

        pred = self.net(X_batch)

        ce = loss_fn(pred, y_batch)

        with torch.no_grad():
          self.ce_acc = self.rho * self.ce_acc + (1 - self.rho) * ce
          coef = self.penalty * self.capacity(self.ce_acc / LOG2)

          ### no chance for the gradient to pass through this...
          c = np.float32(np.maximum(coef.item(), 0.0))

        reg = None
        for param in self.net.parameters():
          if param.ndimension() == 2:
            if reg is None:
              reg = torch.sum(param ** 2)
            else:
              reg = reg + torch.sum(param ** 2)

        loss = ce + c * reg

        loss.backward()
        self.opt.step()

        # epoch_losses[j] = self.ce_acc.item()

        if pbar is not None:
          pbar.update()

      self._loss_history.append(self.ce_acc.item())

      if self.stop_criterion(self._loss_history):
        break

    return self

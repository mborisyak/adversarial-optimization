import numpy as np

import torch
from torch import nn

from .utils import improvement_criterion, seq

__all__ = [
  'Network'
]

class Network(object):
  def __init__(
    self, net, device='cpu',
    stop_criterion=improvement_criterion(),
    max_epoches=1024, batch_size=32,
    min_iterations_per_epoch=128,
  ):
    self.net = net.to(device)
    self.net_init = self.net.state_dict()

    self.opt = torch.optim.Adam(net.parameters(), lr=5e-4)
    self.opt_init = self.opt.state_dict()

    self.device = device

    self._loss_history = list()

    self.batch_size = batch_size
    if isinstance(stop_criterion, int):
      self.stop_criterion = improvement_criterion(stop_criterion)
    else:
      self.stop_criterion = stop_criterion

    self.max_epoches = max_epoches
    self.min_iterations_per_epoch = min_iterations_per_epoch

  def reset(self):
    self.net.load_state_dict(self.net_init)
    self.opt.load_state_dict(self.opt_init)
    self._loss_history = list()

  def fit(self, X, y, reset=True, progress=None):
    if reset:
      self.reset()

    loss_fn = nn.BCEWithLogitsLoss()

    n_iterations = max(X.shape[0] // self.batch_size, self.min_iterations_per_epoch)
    pbar = progress(total=n_iterations) if progress is not None else None

    for i in range(self.max_epoches):
      epoch_losses = np.zeros(shape=(n_iterations,), dtype='float32')

      if pbar is not None:
        pbar.reset(n_iterations)
        pbar.set_description('epoch %d' % (len(self._loss_history),))

      for j in range(n_iterations):
        self.opt.zero_grad()
        indx = np.random.randint(X.shape[0], size=(self.batch_size, ))
        X_batch = torch.tensor(X[indx], dtype=torch.float32, device=self.device, requires_grad=False)
        y_batch = torch.tensor(y[indx], dtype=torch.float32, device=self.device, requires_grad=False)

        pred = self.net(X_batch)
        loss = loss_fn(pred, y_batch)

        epoch_losses[j] = loss.item()

        loss.backward()
        self.opt.step()

        if pbar is not None:
          pbar.update()

      self._loss_history.append(np.mean(epoch_losses))

      if self.stop_criterion(self._loss_history):
        break

    return self

  def loss_history(self):
    return np.array(self._loss_history)

  def predict_proba(self, X, flatten=False):
    result = np.ndarray(shape=(X.shape[0], ), dtype='float32')

    for indx in seq(X.shape[0], self.batch_size):
      X_batch = torch.tensor(X[indx], dtype=torch.float32, device=self.device)
      logits = self.net(X_batch)
      proba = torch.sigmoid(logits)
      result[indx] = proba.detach().cpu().numpy()

    if flatten:
      return result
    else:
      return np.vstack([1 - result, result]).T

  def predict_grid(self, space, steps=20):
    xs = tuple([
      np.linspace(lower_x, upper_x, num=steps)
      for lower_x, upper_x in space
    ])

    grids = np.meshgrid(*xs)
    X = np.vstack([grid.reshape(-1) for grid in grids]).T
    proba = self.predict_proba(X, flatten=True)
    proba = proba.reshape((steps, ) * len(space))

    return xs, proba
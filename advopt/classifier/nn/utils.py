import numpy as np
import torch

__all__ = [
  'lincapacity', 'logcapacity', 'constcapacity',
  'improvement_criterion', 'min_epoches_criterion',
  'seq'
]

def cummin(arr, x0=np.log(2)):
  c = x0
  result = np.zeros_like(arr)

  for i, x in enumerate(arr):
    if x < c:
      result[i] = x
      c = x
    else:
      result[i] = c

  return result

class improvement_criterion(object):
  def __init__(self, epoch_without_improvement=16, eps=1e-4):
    self.epoch_without_improvement = epoch_without_improvement
    self.eps = eps

  def __call__(self, loss_history):
    n = self.epoch_without_improvement

    if len(loss_history) < n + 1:
      return False

    minimal = np.min(loss_history[:-n])

    return all([
      l >= minimal - self.eps
      for l in loss_history[-n:]
    ])

class min_epoches_criterion(object):
  def __init__(self, n_epoches):
    self.n_epoches = n_epoches

  def __call__(self, loss_history):
    return len(loss_history) > self.n_epoches

def seq(n_samples, batch_size=None):
  indx = np.arange(n_samples)

  if batch_size is None:
    for i in indx:
      yield i

  else:
    n_batches = n_samples // batch_size + (1 if n_samples % batch_size != 0 else 0)

    for i in range(n_batches):
      i_from = i * batch_size
      i_to = i_from + batch_size
      yield indx[i_from:i_to]

class constcapacity(object):
  def __init__(self, capacity, device=None):
    self.capacity = torch.tensor(capacity, dtype=torch.float32)

    if device is not None:
      self.capacity.to(device)

  def __call__(self, t):
    return self.capacity

class lincapacity(object):
  def __call__(self, t):
    return 1 - t

class logcapacity(object):
  def __call__(self, t):
    return -torch.log(t)
import torch
from torch import nn

__all__ = [
  'Dense'
]

class Dense(nn.Module):
  def __init__(self, ndim, n0, activation=nn.Softplus()):
    super(Dense, self).__init__()

    self.layer1 = nn.Linear(in_features=ndim, out_features=n0)
    self.layer2 = nn.Linear(in_features=n0, out_features= n0 // 2)
    self.layer3 = nn.Linear(in_features=n0 // 2, out_features=1)

    self.activation = activation

  def forward(self, X):
    X1 = self.activation(self.layer1(X))
    X2 = self.activation(self.layer2(X1))
    return torch.squeeze(self.layer3(X2), dim=1)
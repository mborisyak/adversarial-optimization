#!/bin/bash

python adversarial-optimization/main.py tunemc BO --metrics JSD-NN log-pJSD-NN lin-pJSD-NN --budget=128000 --device=cuda/4 --root=RAO --repeat=20 --seed=$1